<html>
<head>
	<title>Pengisian Form</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="index.css">
</head>
<body>
	<div id="content">
		<header>
			<div id="header"><h1><b>&ensp;&ensp;Pengisian Form Mahasiswa UAD</b></h1></div>
			<div id="menu">
					<ul>
						<li><b><a href="lihatdata.php"><img src="analytics.png" weidth="20px" height="16px"> Lihat Data </a></b></li>
					</ul>
			</div>
		</header>
		<main>
			<article>
				<div id="main-content">
					<div id="news">
                    <script type ="text/javascript">
                    function validasi(data){
                        var nama =(document.data.nama.value);
                        var nim =(document.data.nim.value);
                        var email =(document.data.email.value);
                        var agama =(document.data.agama.value);

                        var ketx = "";
                        if (document.data.Jk.checked == true){
                            ketx = "Laki-laki";
                        }
                        if (document.data.Jk.checked == false && document.data.jk.checked == false || document.data.alamat.value == false ){
                            ketx = "";
                        }else if (document.data.jk.checked == true){
                            ketx = "Perempuan";
                        }
                        var prodi =(document.data.prodi.value);

                        var ket = "";
                        var ket1 = "";
                        var ket2 = "";
                        var ket3 = "";
                        var ket4 = "";
                        if (document.data.hobi1.checked == true){
                        ket = "Membaca";
                        }
                        if (document.data.hobi2.checked == true){
                        ket1 = "Menulis";
                        }
                        if (document.data.hobi3.checked == true){
                        ket2 = "Game";
                        }
                        if (document.data.hobi4.checked == true){
                        ket3 = "Olahraga";
                        }
                        if (document.data.hobi5.checked == true){
                        ket4 = "Programing";
                        }

                        var alamat =(document.data.alamat.value);

                        {if(document.data.nim.value === false || document.data.nama.value == false || document.data.email.value == false || document.data.alamat.value == false){
                            alert ("Nama, NIM, E-mail atau Alamat belum terisi dengan benar, CEK KEMBALI !!! ");
                        }else{
                            alert("Terimakasih, data Anda berhasil dikirim.");
                            alert ("Nama              : " +nama+ 
                                   "\nNIM                : " +nim+ 
                                   "\nE-mail             : " +email+ 
                                   "\nAgama            : " +agama+ 
                                   "\nJenis Kelamin  : " +ketx+ 
                                   "\nProgram Studi : " +prodi+ 
                                   "\nHobi                : " +ket+ " " +ket1+ " " +ket2+ " " +ket3+ " " +ket4+ 
                                   "\nAlamat             : " +alamat);
                            }
                        }
                    }
                    </script>
                    <h2>Lengkapi Biodata Anda di sini</h2>
                    <div class="border">
                        <form name="data" method="post" action="">
                            <table width="100%" border="0">
                                <tr><td width="30%">
                                    Nama</td><td> : <input class="text1" name="nama" type="text" id="nama" size="42" maxlength="25" placeholder="Nama Lengkap">
                                </td></tr>
                                <tr><td>
                                    NIM</td><td> : <input class="text1" name="nim" type="text" id="nim" size="42" maxlength="30" placeholder="NIM">
                                </td></tr>
                                <tr><td>
                                    E-mail</td><td> : <input class="text1" name="email" type="email" id="email" size="42" maxlength="30" placeholder="E-mail">
                                </td></tr>
                                <tr><td>
                                        Agama</td><td> : <select name="agama" id="agama">
                                        <option>Pilih Agama</option>
                                        <option value="Islam">Islam</option>
                                        <option value="Kristen">Kristen</option>
                                        <option value="Hindu">Hindu</option>
                                        <option value="Budha">Budha</option>
                                        <option value="Kong Hu Chu">Kong Hu Chu</option>
                                </select>
                                </td></tr>
                                <tr><td>
                                    Jenis Kelamin</td><td> : <input name="Jk" type="radio" id="Jk" value="Laki-laki"> Laki-laki
                                    <input name="jk" type="radio" id="jk" value="Perempuan"> Perempuan
                                </td></tr>
                                <tr><td>
                                    Program Studi</td><td> : <select name="prodi" id="prodi">
                                        <option>Pilih Prodi</option>
                                        <option value="Teknik Informatika">Teknik Informatika</option>
                                        <option value="Teknik Industri">Teknik Industri</option>
                                        <option value="Teknik Elektro">Teknik Elektro</option>
                                        <option value="Teknik Kimia">Teknik Kimia</option>
                                        <option value="Teknologi Pangan">Teknologi Pangan</option>
                                </select>
                                </td></tr>
                                <tr><td>
                                    Hobi</td><td> : <input type="checkbox" name="hobi1" value="membaca"/>Membaca
                                    <input type="checkbox" name="hobi2" value="menulis"/>Menulis
                                    <input type="checkbox" name="hobi3" value="game"/>Game
                                    <input type="checkbox" name="hobi4" value="olahraga"/>Olahraga
                                    <input type="checkbox" name="hobi5" value="programing"/>Programing
                                </td></tr>
                                <tr><td>
                                    Alamat</td><td> : <textarea name="alamat" id="alamat" cols="20" rows="3" placeholder="Alamat"></textarea>
                                </td></tr>
                                <tr><td colspan="2"><div align="center">
                                    <input class="button2" type="submit" name="" value="Kirim" onClick="validasi(this.data)">
                                    <input class="button2" type="reset" name="" value="Ulangi">
                                </div></td></tr>
                            </table>
                        </form>
                    </div>
					</div>
				</div>
			</article>
			<aside>
				<div id="side-menu">
					<h3>Menu</h3>
					<ul>
						<li><span class="span-text"><b><a href="index.php">Home</a></b></span></li>
						<li><span class="span-text"><b><a href="biodata.php">Input Biodata</a></b></span></li>
						<li><span class="span-text"><b><a href="nilai.php">Input Nilai</a></b></span></li>
					</ul>
				</div>
			</aside>
		</main>
		<footer>
			<p class="footer-text">Copyrigth © | 2022 By <span class="span-text">Aditya Prariyadi Wibowo.</span> All right reserved.</p>
		</footer>
	<div>
</body>
</html>