<html>
<head>
	<title>Pengisian Form</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="index.css">
</head>
<body>
	<div id="content">
		<header>
			<div id="header"><h1><b>&ensp;&ensp;Pengisian Form Mahasiswa UAD</b></h1></div>
			<div id="menu">
					<ul>
						<li><b><a href="lihatdata.php"><img src="analytics.png" weidth="20px" height="16px"> Lihat Data </a></b></li>
					</ul>
			</div>
		</header>
		<main>
			<article>
				<div id="main-content">
					<div id="news">
                    <h3 style="margin-left:1pc;">Data Rekap Nilai Mahasiswa UAD 2021/2022 </h3>
                    <?php
                        $fp = fopen("data.txt", "r");
                        echo("<table width='90%' bgcolor='grey' cellspacing='1' style='margin-left:1pc'>");
                        echo("<tr bgcolor='white'>");
                        echo("<th width='100'>NIM</th>");
                        echo("<th>Nama Mahasiswa</th>");
                        echo("<th width='100'>Nilai Absensi</th>");
                        echo("<th width='100'>Nilai Tugas</th>");
                        echo("<th width='100'>Nilai UTS</th>");
                        echo("<th width='100'>Nilai UAS</th>");
                        echo("<th width='100'>Grade</th>");
                        echo("</tr>");
                        while ($isi = fgets($fp, 1000)){
                            $pisah = explode("|",$isi);
                            echo("<tr bgcolor='white'>");
                            echo("<td align='center'>$pisah[0]</td>");
                            echo("<td>$pisah[1]</td>");
                            echo("<td align='center'>$pisah[2]</td>");
                            echo("<td align='center'>$pisah[3]</td>");
                            echo("<td align='center'>$pisah[4]</td>");
                            echo("<td align='center'>$pisah[5]</td>");
                            echo("<td align='center'>$pisah[6]</td>");
                            echo("</tr>");
                        }
                        echo "</table>";
                    ?>
					</div>
				</div>
			</article>
			<aside>
				<div id="side-menu">
					<h3>Menu</h3>
					<ul>
						<li><span class="span-text"><b><a href="index.php">Home</a></b></span></li>
						<li><span class="span-text"><b><a href="biodata.php">Input Biodata</a></b></span></li>
						<li><span class="span-text"><b><a href="nilai.php">Input Nilai</a><b></span></li>
					</ul>
				</div>
			</aside>
		</main>
		<footer>
			<p class="footer-text">Copyrigth © | 2022 By <span class="span-text">Aditya Prariyadi Wibowo.</span> All right reserved.</p>
		</footer>
	<div>
</body>
</html>
