<html>
<head>
	<title>Pengisian Form</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="index.css">
</head>
<body>
	<div id="content">
		<header>
			<div id="header"><h1><b>&ensp;&ensp;Pengisian Form Mahasiswa UAD</b></h1></div>
			<div id="menu">
					<ul>
                        <li><b><a href="lihatdata.php"><img src="analytics.png" weidth="20px" height="16px"> Lihat Data </a></b></li>
					</ul>
			</div>
		</header>
		<main>
			<article>
				<div id="main-content">
					<div id="news">
                    <script type="text/javascript">
						function validasi(){
						var beli=document.getElementById("nim").value;
						var nama=document.getElementById("nama").value;
						var menu=document.getElementById("jk").value;
						var harga=document.getElementById("jp").value;
					    var email=document.getElementById("nilaitugas").value;
						var telp=document.getElementById("nilaiuts").value;
						var alamat=document.getElementById("nilaiuas").value;
						if(nim !="" && nama !="" && jk !="" && jp !="" && nilaitugas !="" && nilaiuts!="" && nilaiuas!=""){
							return true;
						}else{
							alert("Anda harus mengisi data dengan lengkap!");
						}
					}
					</script>
                    <h2>Silahkan Isi Data Berikut</h2>
                    <div class="border">
                        <form name="form1" enctype="multipart/form-data" method="post" action="" onSubmit="validasi()">
                            <table width="100%" border="0">
                                <tr><td width="30%">
                                    Masukkan NIM</td><td> : <input class="text1" name="nim" type="text" id="nim" size="42" maxlength="25" placeholder="NIM">
                                </td></tr>
                                <tr><td>
                                    Masukkan Nama</td><td> : <input class="text1" name="nama" type="text" id="nama" size="42" maxlength="30" placeholder="Nama Lengkap">
                                </td></tr>
                                <tr><td>
                                    Masukkan Jumlah Kehadiran</td><td> : <input class="text1" name="jk" type="text" id="jk" size="42" maxlength="30" placeholder="10">
                                </td></tr>
                                <tr><td>
                                    Masukkan Jumlah Pertemuan</td><td> : <input class="text1" name="jp" type="text" id="jp" size="42" maxlength="30" placeholder="10">
                                </td></tr>
                                <tr><td>
                                    Masukkan Nilai Tugas</td><td> : <input class="text1" name="nilaitugas" type="text" id="nilaitugas" size="42" maxlength="30" placeholder="0-100">
                                </td></tr>
                                <tr><td>
                                    Masukkan Nilai UTS</td><td> : <input class="text1" name="nilaiuts" type="text" id="nilaiuts" size="42" maxlength="30" placeholder="0-100">
                                </td></tr>
                                <tr><td>
                                    Masukkan Nilai UAS</td><td> : <input class="text1" name="nilaiuas" type="text" id="nilaiuas" size="42" maxlength="30" placeholder="0-100">
                                </td></tr>
                                <tr><td colspan="2">
                                    <input class="button1" type="submit" name="submit" value="Simpan Data">
                                </td></tr>
                            </table>
                        </form>
                    </div>
                    <?php
                        if(isset($_POST['submit'])){
                            $fp = fopen("data.txt", "a+"); 
                            $nim  = $_POST["nim"];
                            $nama  = $_POST["nama"];
                            $jk  = $_POST["jk"];
                            $jp  = $_POST["jp"];
                            $tugas  = $_POST["nilaitugas"];
                            $uts  = $_POST["nilaiuts"];
                            $uas  = $_POST["nilaiuas"];

                            function grade($nilai){
                                if($nilai <= 100 ){ 
                                    $grade = "A"; 
                                }else if($nilai <  80 ){ 
                                    $grade = "B"; 
                                }else if($nilai <  70 ){ 
                                    $grade = "C"; 
                                }else if($nilai <  60 ){ 
                                    $grade = "D"; 
                                }else if($nilai <  50 ){ 
                                    $grade = "E"; 
                                }

                                return $grade;
                            }
                            
                            $absensi= ($jk/$jp)*100;
                            $nilai  = ($absensi*0.1)+ ($tugas*0.2)+ ($uts*0.3)+ ($uas*0.4);
                            $grade  = grade($nilai);
                            fputs($fp, "$nim|$nama|$absensi|$tugas|$uts|$uas|$grade\n");
                            fclose($fp);
                        }
                    ?>
					</div>
				</div>
			</article>
			<aside>
				<div id="side-menu">
					<h3>Menu</h3>
					<ul>
						<li><span class="span-text"><b><a href="index.php">Home</a></b></span></li>
						<li><span class="span-text"><b><a href="biodata.php">Input Biodata</a></b></span></li>
						<li><span class="span-text"><b><a href="nilai.php">Input Nilai</a></b></span></li>
					</ul>
				</div>
			</aside>
		</main>
		<footer>
			<p class="footer-text">Copyrigth © | 2022 By <span class="span-text">Aditya Prariyadi Wibowo.</span> All right reserved.</p>
		</footer>
	<div>
</body>
</html>
