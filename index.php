<html>
<head>
	<title>Pengisian Form</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="index.css">
</head>
<body>
	<div id="content">
		<header>
			<div id="header"><h1><b>&ensp;&ensp;Pengisian Form Mahasiswa UAD</b></h1></div>
			<div id="menu">
					<ul>
						<li><b><a href="lihatdata.php"><img src="analytics.png" weidth="20px" height="16px"> Lihat Data </a></b></li>
					</ul>
			</div>
		</header>
		<main>
			<article>
				<div id="main-content">
					<div id="news">
						<img src="https://news.uad.ac.id/wp-content/uploads/Kampus-4-Universitas-Ahmad-Dahlan.jpg"  width="90%" height="50%">
						<h2><b>Profil Singkat Universitas Ahmad Dahlan (UAD)</b></h2>
						<p align="justify">Universitas Ahmad Dahlan (UAD) adalah sebuah perguruan tinggi swasta yang berdiri di Yogyakarta. 
						Universitas Ahmad Dahlan berdiri pada tanggal 18 November 1960 pengembangan dari Institut Keguruan dan llmu Pendidikan (IKIP) Muhammadiyah Yogyakarta. 
						Universitas Ahmad Dahlan memiliki 11 Fakultas, Program Vokasi, Program Profesi, dan Program Pascasarjana.</p>
						<h2><b>Tujuan Dari Pengisian Form</b></h2>
						<p>Tujuan dari pengisian formulir yaitu untuk memberikan informasi yang jelas dan sebenarnya tentang diri anda kepada Instansi atau organisasi yang nantinya
						digunakan untuk menginputkan biodata dan nilai anda.</p>
					</div>
				</div>
			</article>
			<aside>
				<div id="side-menu">
					<h3>Menu</h3>
					<ul>
						<li><span class="span-text"><b><a href="index.php">Home</a></b></span></li>
						<li><span class="span-text"><b><a href="biodata.php">Input Biodata</a></b></span></li>
						<li><span class="span-text"><b><a href="nilai.php">Input Nilai</a></b></span></li>
					</ul>
				</div>
			</aside>
		</main>
		<footer>
			<p class="footer-text">Copyrigth © | 2022 By <span class="span-text">Aditya Prariyadi Wibowo.</span> All right reserved.</p>
		</footer>
	<div>
</body>
</html>
